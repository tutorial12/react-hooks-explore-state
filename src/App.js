import React, {useState,useReducer, useContext, createContext} from 'react';
import {v4} from 'uuid';

const DispatchContext = createContext(null);

const initialTodos = [
  {
    id: v4(),
    task: 'Learn React',
    complete: true,
  },
  {
    id: v4(),
    task: 'Learn Firebase',
    complete: true,
  },
  {
    id: v4(),
    task: 'Learn GraphQL',
    complete: false,
  },
];

const todoReducer = (state,action) => {
  switch(action.type) {
    case 'DO_TODO':
      return state.map(todo => {
        if(todo.id === action.id) {
          return {...todo,complete:true};
        } else {
          return todo;
        }
      });
    case 'UNDO_TODO':
      return state.map(todo => {
        if(todo.id === action.id) {
          return {...todo, complete:false};
        } else {
          return todo;
        }
      });
    case 'ADD_TODO':
      return state.concat({
        task: action.task,
        id: action.id,
        complete: false,
      });
    default:
      return state;
  }
};

const filterReducer = (state,action) => {
  switch (action.type) {
    case 'SHOW ALL':
      return 'ALL';
    case 'SHOW COMPLETE':
      return 'COMPLETE';
    case 'SHOW INCOMPLETE':
      return 'INCOMPLETE';
    default:
      return state;
  }
};

const useCombinedReducer = useReducer => {
  // Global state
  const state = Object.keys(useReducer).reduce((acc,key)=>({...acc,[key]:useReducer[key][0]}),{});
  // Global dispatch
  const dispatch = action => Object.keys(useReducer).map(key=>useReducer[key][1]).forEach(fn=>fn(action));
  return [state,dispatch];
};

const Filter = () => {
  const dispatch = useContext(DispatchContext);
  const handleShowAll = () => {
    dispatch({type:'SHOW ALL'});
  };

  const handleShowComplete = () => {
    dispatch({type:'SHOW COMPLETE'});
  };

  const handleShowIncomplete = () => {
    dispatch({type:'SHOW INCOMPLETE'});
  };
  return (<div>
    <button type="button" onClick={handleShowAll}>Show All</button>
    <button type="button" onClick={handleShowComplete}>Show Complete</button>
    <button type="button" onClick={handleShowIncomplete}>Show Incomplete</button>
  </div>);
};

const TodoItem = ({todo}) => {
  const dispatch = useContext(DispatchContext);
  const handleChange = () =>
    dispatch({
      type:todo.complete? 'UNDO_TODO':'DO_TODO',
      id:todo.id,
    });
    return (
    <li>
      <label>
      <input
        type="checkbox"
        checked={todo.complete}
        onChange={handleChange}
      />
      {todo.task}
      </label>
    </li>)
};

const TodoList = ({todos}) => (
  <ul>
    {todos.map(todo => (<TodoItem key={todo.id} todo={todo}/>))}
  </ul>
);

const AddTodo = () => {
  const dispatch = useContext(DispatchContext);
  const [task,setTask] = useState('');

  const handleSubmit = event => {
    event.preventDefault();
    if(task) {
      dispatch({type:'ADD_TODO',task,id:v4()});
    }
    setTask('');
  }
  const handleChange = event => setTask(event.target.value);
  return (
    <form onSubmit={handleSubmit}>
      <input type="test" value={task} onChange={handleChange}/>
      <button type="submit">Add Todo</button>
    </form>
  )
};

const App = () => {
  // const [todos,dispatchTodos] = useReducer(todoReducer,initialTodos);
  // const [filter, dispatchFilter] = useReducer(filterReducer,'ALL');
  // const dispatch = action => [dispatchTodos,dispatchFilter].forEach(fn => fn(action));
  const [state,dispatch] = useCombinedReducer({
    filter:useReducer(filterReducer,'ALL'),
    todos:useReducer(todoReducer,initialTodos),
  });

  const {filter,todos} = state;


  const filteredTodos = todos.filter(todo => {
    if(filter === 'ALL') {
      return true;
    }
    if(filter === 'COMPLETE' && todo.complete) {
      return true;
    }
    if(filter === 'INCOMPLETE' && !todo.complete) {
      return true;
    }
    return false;
  });

  return (
  <DispatchContext.Provider value={dispatch}>
    <Filter/>
    <TodoList todos={filteredTodos}/>
    <AddTodo />
  </DispatchContext.Provider>
  )
};
 
export default App;